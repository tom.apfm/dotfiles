#+BEGIN_QUOTE
 bash           > bash settings, aliases, and custom prompts
 emacs          > for everything!!
 vim            > cli based text editor -> the ultimate weapon of choice
 git            > global git config and aliases

and more
#+END_QUOTE

* installing

** stow

- =sudo pacman -S stow=
- =sudo apt-get install stow=
- =brew install stow=

** dotfiles

- =stow emacs -t ~=
- =stow bash -t ~=
- and so on
first, install hack font
https://github.com/source-foundry/Hack#quick-installation
