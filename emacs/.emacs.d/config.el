(setq user-full-name "Yizhe Xu"
      user-mail-address "me@yizhexu.com"
      calendar-latitude 47.6133788
      calendar-longitude -122.3497462)

(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")))
(package-initialize)

;(setq load-prefer-newer t)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq use-package-always-ensure t)

(use-package paradox
  :config
  (setq paradox-execute-asynchronously t))

(defun is-mac-p
    ()
  (eq system-type 'darwin))

(defun is-linux-p
    ()
  (eq system-type 'gnu/linux))

(defun is-windows-p
    ()
  (or
   (eq system-type 'ms-dos)
   (eq system-type 'windows-nt)
   (eq system-type 'cygwin)))

(defun is-bsd-p
    ()
  (eq system-type 'gnu/kfreebsd))

(defun is-yizhx
    ()
  (string-equal user-login-name "yizhx"))

(defun internet-up-p (&optional host)
  (= 0 (call-process "ping" nil nil nil "-c" "1" "-W" "1"
                     (if host host "www.google.com"))))

(defun transparency (value)
        "set transparency of the frame window. 0=transparent/ 100=opaque"
        (interactive "nTransparency Value 0-100 opaque:")
        (set-frame-parameter (selected-frame) 'alpha value))

(use-package nord-theme
  :config
  (load-theme 'nord t)
        (transparency 93)
        (let ((line (face-attribute 'mode-line :underline)))
    (set-face-attribute 'mode-line          nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :overline   line)
    (set-face-attribute 'mode-line-inactive nil :underline  line)
    (set-face-attribute 'mode-line          nil :box        nil)
    (set-face-attribute 'mode-line-inactive nil :box        nil)))

(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(setq inhibit-startup-message t
      initial-scratch-message ""
      inhibit-startup-echo-area-message t)

(set-default-font "Hack-12")

(global-prettify-symbols-mode t)

(global-prettify-symbols-mode +1)

(use-package neotree
  :bind ([f8] . neotree-toggle))

(setq-default mode-line-format
              '("%e" ; print error message about full memory.
                mode-line-front-space
                ;; mode-line-mule-info
                ;; mode-line-client
                ;; mode-line-modified
                ;; mode-line-remote
                ;; mode-line-frame-identification
                mode-line-buffer-identification
                "   "
                ;; mode-line-position
                ;; (vc-mode vc-mode)
                ;; "  "
                ;; mode-line-modes
                "   "
                ;; mode-line-misc-info
                ;; battery-mode-line-string
                mode-line-end-spaces))

(setq display-time-24hr-format t
      display-time-format "%a, %b %e %R"
      battery-mode-line-format "%p%%"  ; Default: "[%b%p%%]"
      global-mode-string   (remove 'display-time-string global-mode-string)
      mode-line-end-spaces (list (propertize " "
                                             'display '(space :align-to (- right 17)))
                                 'display-time-string))
(display-time-mode 1)
(display-time-update)

(when (is-mac-p)
  (display-battery-mode 1))

(fset 'yes-or-no-p 'y-or-n-p)

(setq-default indent-tabs-mode nil)

(setq fill-column 80)

(setq confirm-nonexistent-file-or-buffer nil)

(defun create-non-existent-directory ()
  "Check whether a given file's parent directories exist; if they do not, offer to create them."
  (let ((parent-directory (file-name-directory buffer-file-name)))
    (when (and (not (file-exists-p parent-directory))
               (y-or-n-p (format "Directory `%s' does not exist! Create it?" parent-directory)))
      (make-directory parent-directory t))))

(add-to-list 'find-file-not-found-functions #'create-non-existent-directory)

(setq minibuffer-prompt-properties
      (quote
       (read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)))

(use-package smartparens
  :bind
  (("C-M-f" . sp-forward-sexp)
   ("C-M-b" . sp-backward-sexp)
   ("C-M-d" . sp-down-sexp)
   ("C-M-a" . sp-backward-down-sexp)
   ("C-S-a" . sp-beginning-of-sexp)
   ("C-S-d" . sp-end-of-sexp)
   ("C-M-e" . sp-up-sexp)
   ("C-M-u" . sp-backward-up-sexp)
   ("C-M-t" . sp-transpose-sexp)
   ("C-M-n" . sp-next-sexp)
   ("C-M-p" . sp-previous-sexp)
   ("C-M-k" . sp-kill-sexp)
   ("C-M-w" . sp-copy-sexp)
   ("M-<delete>" . sp-unwrap-sexp)
   ("M-S-<backspace>" . sp-backward-unwrap-sexp)
   ("C-<right>" . sp-forward-slurp-sexp)
   ("C-<left>" . sp-forward-barf-sexp)
   ("C-M-<left>" . sp-backward-slurp-sexp)
   ("C-M-<right>" . sp-backward-barf-sexp)
   ("M-D" . sp-splice-sexp)
   ("C-M-<delete>" . sp-splice-sexp-killing-forward)
   ("C-M-<backspace>" . sp-splice-sexp-killing-backward)
   ("C-M-S-<backspace>" . sp-splice-sexp-killing-around)
   ("C-]" . sp-select-next-thing-exchange)
   ("C-<left_bracket>" . sp-select-previous-thing)
   ("C-M-]" . sp-select-next-thing)
   ("M-F" . sp-forward-symbol)
   ("M-B" . sp-backward-symbol)
   ("H-t" . sp-prefix-tag-object)
   ("H-p" . sp-prefix-pair-object)
   ("H-s c" . sp-convolute-sexp)
   ("H-s a" . sp-absorb-sexp)
   ("H-s e" . sp-emit-sexp)
   ("H-s p" . sp-add-to-previous-sexp)
   ("H-s n" . sp-add-to-next-sexp)
   ("H-s j" . sp-join-sexp)
   ("H-s s" . sp-split-sexp)
   ("M-9" . sp-backward-sexp)
   ("M-0" . sp-forward-sexp))
  :init
  (smartparens-global-mode t)
  (show-smartparens-global-mode t)
  (use-package smartparens-config
    :ensure f)
  ;(bind-key "s" 'smartparens-mode toggle-map)
  (when (is-mac-p)
    (bind-keys ("<s-right>" . sp-forward-slurp-sexp)
               ("<s-left>" . sp-forward-barf-sexp)))
  (sp-with-modes '(markdown-mode gfm-mode)
    (sp-local-pair "*" "*"))
  (sp-with-modes '(org-mode)
    (sp-local-pair "*" "*")
    (sp-local-pair "=" "=")
    (sp-local-pair "/" "/")
    (sp-local-pair "(" ")")
    (sp-local-pair "[" "]"))
  (use-package rainbow-delimiters
    :hook (prog-mode . rainbow-delimiters-mode)))

(add-hook 'before-save-hook 'whitespace-cleanup)

(use-package hydra
   :config
   (setq hydra-lv nil))

(defhydra hydra-zoom ()
  "zoom"
  ("+" text-scale-increase "in")
  ("=" text-scale-increase "in")
  ("-" text-scale-decrease "out")
  ("_" text-scale-decrease "out")
  ("0" (text-scale-adjust 0) "reset")
  ("q" nil "quit" :color blue))

(bind-keys ("C-x C-0" . hydra-zoom/body)
           ("C-x C-=" . hydra-zoom/body)
           ("C-x C--" . hydra-zoom/body)
           ("C-x C-+" . hydra-zoom/body))

(set-frame-parameter nil 'fullscreen 'fullboth)

(defun vsplit-last-buffer ()
  (interactive)
  (split-window-vertically)
  (other-window 1 nil)
  (switch-to-next-buffer))

(defun hsplit-last-buffer ()
  (interactive)
  (split-window-horizontally)
  (other-window 1 nil)
  (switch-to-next-buffer))

(bind-key "C-x 2" 'vsplit-last-buffer)
(bind-key "C-x 3" 'hsplit-last-buffer)

(setq tls-checktrust t
      gnutls-verify-error t)

(setenv "GPG_AGENT_INFO" nil)

(use-package pass)

(let ((backup-dir "~/Documents/backups")
      (auto-saves-dir "~/Documents/auto-saves/"))
  (dolist (dir (list backup-dir auto-saves-dir))
    (when (not (file-directory-p dir))
      (make-directory dir t)))
  (setq backup-directory-alist `(("." . ,backup-dir))
        auto-save-file-name-transforms `((".*" ,auto-saves-dir t))
        auto-save-list-file-prefix (concat auto-saves-dir ".saves-")
        tramp-backup-directory-alist `((".*" . ,backup-dir))
        tramp-auto-save-directory auto-saves-dir))

(setq backup-by-copying t    ; Don't delink hardlinks
      delete-old-versions t  ; Clean up the backups
      version-control t      ; Use version numbers on backups,
      kept-new-versions 5    ; keep some new versions
      kept-old-versions 2)   ; and some old ones, too

(setq-default save-place t)
(setq save-place-file (expand-file-name ".places" user-emacs-directory))

(save-place-mode 1)

(bind-keys ("RET" . newline-and-indent)
           ("C-j" . newline-and-indent))

(setq next-line-add-newlines t)

(defun super-next-line ()
  (interactive)
  (ignore-errors (next-line 5)))

(defun super-previous-line ()
  (interactive)
  (ignore-errors (previous-line 5)))

(defun super-backward-char ()
  (interactive)
  (ignore-errors (backward-char 5)))

(defun super-forward-char ()
  (interactive)
  (ignore-errors (forward-char 5)))

(bind-keys ("C-S-n" . super-next-line)
           ("C-S-p" . super-previous-line)
           ("C-S-b" . super-backward-char)
           ("C-S-f" . super-forward-char))

(bind-key "C-<backspace>" (lambda ()
                            (interactive)
                            (kill-line 0)
                            (indent-according-to-mode)))

(bind-key "C-x SPC" 'cycle-spacing)

(when (is-mac-p)
  (setq mac-command-modifier 'meta
        mac-option-modifier 'super
        mac-control-modifier 'control
        ns-function-modifier 'hyper))

(use-package which-key
  :init
  (which-key-mode))

(use-package discover-my-major
  :bind ("C-h C-m" . discover-my-major))

(use-package interaction-log)

(interaction-log-mode +1)

(defun open-interaction-log ()
  (interactive)
  (display-buffer ilog-buffer-name))

(bind-key "C-h C-l" 'open-interaction-log)

(defun yizhe/append-to-path (path)
  "Add a path both to the $PATH variable and Emacs's path"
  (setenv "PATH" (concat (getenv "PATH") ":" path))
  (add-to-list 'exec-path path))

(yizhe/append-to-path "/usr/bin")
(yizhe/append-to-path "/usr/local/bin")

(use-package company
  :bind (("C-." . company-complete)
         :map company-active-map
         ("C-n" . company-select-next)
         ("C-p" . company-select-previous)
         ("C-d" . company-show-doc-buffer)
         ("<tab>" . company-complete))
  :init
  (global-company-mode 1)
  :config
  (setq company-show-numbers t
        company-tooltip-align-annotations t)

  (let ((map company-active-map))
    (mapc
     (lambda (x)
       (define-key map (format "%d" x) 'ora-company-number))
     (number-sequence 0 9))
    (define-key map " " (lambda ()
                          (interactive)
                          (company-abort)
                          (self-insert-command 1)))
    (define-key map (kbd "<return>") nil))

  (defun ora-company-number ()
    "Forward to `company-complete-number'.

Unless the number is potentially part of the candidate.
In that case, insert the number."
    (interactive)
    (let* ((k (this-command-keys))
           (re (concat "^" company-prefix k)))
      (if (cl-find-if (lambda (s) (string-match re s))
                      company-candidates)
          (self-insert-command 1)
        (company-complete-number (string-to-number k))))))

(add-hook 'sh-mode-hook
          (lambda ()
            (setq sh-basic-offset 2
                  sh-indentation 2)))

(setq-default explicit-shell-file-name "bash")

(use-package eshell
  :bind (("<f1>" . eshell))
  :hook ((eshell-mode . with-editor-export-editor)
         (eshell-mode . setup-company-eshell-autosuggest))
  :init
  (setq eshell-banner-message "")

  (defun new-eshell ()
    (interactive)
    (eshell 'true))

  (use-package esh-autosuggest
    :init
    (defun setup-company-eshell-autosuggest ()
      (with-eval-after-load 'company
        (setq-local company-backends '(esh-autosuggest))
        (setq-local company-frontends '(company-preview-frontend))))))

(defun eshell/extract (file)
  (eshell-command-result (concat (if-string-match-then-result
                                  file
                                  '((".*\.tar.bz2" "tar xjf")
                                    (".*\.tar.gz" "tar xzf")
                                    (".*\.bz2" "bunzip2")
                                    (".*\.rar" "unrar x")
                                    (".*\.gz" "gunzip")
                                    (".*\.tar" "tar xf")
                                    (".*\.tbz2" "tar xjf")
                                    (".*\.tgz" "tar xzf")
                                    (".*\.zip" "unzip")
                                    (".*\.jar" "unzip")
                                    (".*\.Z" "uncompress")
                                    (".*" "echo 'Could not extract the requested file:'")))
                                 " " file)))

(defun eshell/clear ()
  "clear the eshell buffer."
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)))

(use-package sbt-mode
  :pin melpa
  :commands sbt-start sbt-command)

(use-package scala-mode
  :pin melpa
  :interpreter ("scala" . scala-mode))

(use-package ensime
  :init
  (put 'ensime-auto-generate-config 'safe-local-variable #'booleanp)
  (setq
    ensime-startup-snapshot-notification nil
    ensime-startup-notification nil))

(use-package ess
 :ensure t
 :init (require 'ess-site))

(setq ess-use-company t
                        company-selectionw-rap-around t
                        company-tooltip-align-annotations t
                        company-idle-delay 0.36
                        company-show-numbers t
                        company-tooltip-flip-when-above t
                        company-minimum-prefix-length 2
                        company-tooltip-limit 10)

(define-key company-active-map (kbd "M-h") 'company-show-doc-buffer)

(define-key company-active-map [return] nil)
(define-key company-active-map [tab] 'company-complete-common)
(define-key company-active-map (kbd "TAB") 'company-complete-common)
(define-key company-active-map (kbd "M-TAB") 'company-complete-selection)

(define-key company-active-map (kbd "M-n") nil)
(define-key company-active-map (kbd "M-p") nil)
(define-key company-active-map (kbd "M-,") 'company-select-next)
(define-key company-active-map (kbd "M-k") 'company-select-previous)

(setq org-confirm-babel-evaluate nil)

(add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
(add-hook 'org-mode-hook 'org-display-inline-images)

(when (is-linux-p) (yizhe/append-to-path "/opt/anaconda/bin"))
(when (is-mac-p) (yizhe/append-to-path "~~/.pyenv/shims/python"))

; where to look for environments
(when (is-linux-p)(setenv "WORKON_HOME" "/home/yizhe/.conda/envs"))
(when (is-mac-p)(setenv "WORKON_HOME" "~/.local/share/virtualenvs"))

(use-package pyvenv
        :init
        (pyvenv-mode 1)
        (pyvenv-tracking-mode 1))

;; enable elpy
(use-package python
  :defer t
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode)
  :init
  (setq-default indent-tabs-mode nil)
  :config
  (setq python-indent-offset 4)
  (use-package smartparens
    :init
    (add-hook 'python-mode-hook 'smartparens-mode))
  (use-package color-identifiers-mode
    :init
    (add-hook 'python-mode-hook 'color-identifiers-mode)))

(use-package elpy
  :init (add-hook 'python-mode-hook 'elpy-enable))

(setq python-indent-guess-indent-offset t)
(setq python-indent-guess-indent-offset-verbose nil)

(setq python-shell-completion-native-enable nil)

;; ipython interpreter
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt")

(use-package py-autopep8
:init
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save))

(use-package elpy
  :ensure t
  :commands elpy-enable
  :init (with-eval-after-load 'python (elpy-enable))

  :config
  (electric-indent-local-mode -1)
  (delete 'elpy-module-highlight-indentation elpy-modules)
  (delete 'elpy-module-flymake elpy-modules)

  (defun ha/elpy-goto-definition ()
    (interactive)
    (condition-case err
        (elpy-goto-definition)
      ('error (xref-find-definitions (symbol-name (symbol-at-point))))))

  :bind (:map elpy-mode-map ([remap elpy-goto-definition] .
                             ha/elpy-goto-definition)))

(add-hook 'elpy-mode-hook
            (lambda ()
                    (setq-default indent-tabs-mode t)
                    (setq-default tab-width 2)
                    (setq-default py-indent-tabs-mode t)
            (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

(use-package jedi
  :config
  (use-package company-jedi
    :init
    (add-hook 'python-mode-hook (lambda () (add-to-list 'company-backends 'company-jedi)))
    (setq company-jedi-python-bin "python")))

(when (is-mac-p)
  (yizhe/append-to-path "/Applications/Julia-1.0.app/Contents/Resources/julia/bin"))

(use-package julia-repl)

(use-package ob-julia
  :load-path "~/.emacs.d/resources")

(use-package re-builder
  :bind (("C-c R" . re-builder))
  :config
  (setq reb-re-syntax 'string))

(use-package visual-regexp
    :bind (("M-5" . vr/replace)
           ("M-%" . vr/query-replace)))

(use-package elisp-slime-nav
  :init
  (dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
    (add-hook hook 'elisp-slime-nav-mode)))

(autoload 'turn-on-eldoc-mode "eldoc" nil t)
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'eldoc-mode)
(add-hook 'ielm-mode-hook 'eldoc-mode)
(add-hook 'cider-mode-hook 'eldoc-mode)

(use-package magit
  :bind (("C-x g" . magit-status)
         ("C-c g" . magit-status)
         :map magit-status-mode-map
         ("TAB" . magit-section-toggle)
         ("<C-tab>" . magit-section-cycle)
         :map magit-branch-section-map
         ("RET" . magit-checkout))
  :config
  (add-hook 'after-save-hook 'magit-after-save-refresh-status)
  (setq magit-use-overlays nil
        magit-section-visibility-indicator nil
        magit-completing-read-function 'ivy-completing-read
        magit-push-always-verify nil
        magit-repository-directories '("~/src/"))
  (use-package git-timemachine
    :bind (("C-x v t" . git-timemachine)))
  (use-package git-link
    :bind (("C-x v L" . git-link))
    :init
    (setq git-link-open-in-browser t))
  (use-package pcmpl-git)
  (defun visit-pull-request-url ()
    "Visit the current branch's PR on Github."
    (interactive)
    (browse-url
     (format "https://github.com/%s/pull/new/%s"
             (replace-regexp-in-string
              "\\`.+github\\.com:\\(.+\\)\\.git\\'" "\\1"
              (magit-get "remote"
                         (magit-get-remote)
                         "url"))
             (cdr (magit-get-remote-branch)))))

  (bind-key "v" 'visit-pull-request-url magit-mode-map)

  ;; Do Not Show Recent Commits in status window
  ;; https://github.com/magit/magit/issues/3230#issuecomment-339900039
  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-unpushed-to-upstream
                          'magit-insert-unpushed-to-upstream-or-recent
                          'replace))

(use-package git-auto-commit-mode
  :delight)

;; I want .hql and .q files to use sql-mode
(defun my-sql-customisations ()
  "sql-mode customisations that must be done after sql-mode loads"
  (add-to-list 'same-window-buffer-names "*SQL*"))

(use-package sql
  :config
  (add-to-list 'auto-mode-alist '("\\.hql\\'" . sql-mode))
  (autoload 'sql-mode "sql-mode" "SQL editing mode." t)
  (setq sql-mode-hook 'my-sql-customisations))

(use-package sql-indent
  :config
(add-hook 'sql-mode-hook 'sqlind-minor-mode))

(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (toggle-truncate-lines t)))

(defun upcase-sql-keywords ()
  (interactive)
  (save-excursion
    (dolist (keywords sql-mode-postgres-font-lock-keywords)
      (goto-char (point-min))
      (while (re-search-forward (car keywords) nil t)
        (goto-char (+ 1 (match-beginning 0)))
        (when (eql font-lock-keyword-face (face-at-point))
          (backward-char)
          (upcase-word 1)
          (forward-char))))))

(when (is-mac-p) (yizhe/append-to-path "/Library/TeX/texbin/"))

(use-package graphviz-dot-mode)

(setq tramp-auto-save-directory "~/tmp")
(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")

(use-package org
  :bind (("C-c l" . org-store-link)
         ("C-c c" . org-capture)
         ("C-c a" . org-agenda)
         ("C-c b" . org-iswitchb)
         ("C-c M-k" . org-cut-subtree)
         :map org-mode-map
         ("C-c >" . org-time-stamp-inactive))
  :custom-face
  (variable-pitch ((t (:family "ETBembo"))))
  (org-done ((t (:strike-through t ))))
  (org-headline-done ((t ( :strike-through t))))
  (org-image-actual-width '(600))
  :init
  (setq default-major-mode 'org-mode
        org-directory "~/Documents/megrez/"
        org-log-done t
        org-startup-indented t
        org-startup-truncated nil
        org-startup-with-inline-images t
        org-completion-use-ido t
        org-default-notes-file (concat org-directory "todo.org")
        org-image-actual-width '(300)
        org-goto-max-level 10
        org-imenu-depth 5
        org-goto-interface 'outline-path-completion
        org-outline-path-complete-in-steps nil
        org-src-fontify-natively t
        org-confirm-babel-evaluate nil
        org-src-preserve-indentation t
        org-lowest-priority ?C
        org-default-priority ?B
        org-expiry-inactive-timestamps t
        org-show-notification-handler 'message
        org-special-ctrl-a/e t
        org-special-ctrl-k t
        org-yank-adjusted-subtrees t
        org-src-window-setup 'current-window
        org-file-apps
        '((auto-mode . emacs)
          ("\\.mm\\'" . default)
          ("\\.x?html?\\'" . "firefox %s")
          ("\\.pdf\\'" . "open %s"))
        org-todo-keywords
        '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "SOMEDAY(.)" "MAYBE(m)" "|" "DONE(x!)" "CANCELLED(c)"))
        ;; Theming
        org-ellipsis "⤵" ;; foldings symbol
        org-pretty-entities t
        org-use-sub-superscripts '{} ;; underscores, etc needs to be wraped as a_{underscore}
        org-hide-emphasis-markers t ;; show actually italicized text instead of /italicized text/
        org-agenda-block-separator ""
        org-fontify-whole-heading-line t
        org-fontify-done-headline t
        org-fontify-quote-and-verse-blocks t
        org-format-latex-options
        (plist-put org-format-latex-options :scale 1.5))

  (add-to-list 'org-global-properties
               '("Effort_ALL". "0:05 0:15 0:30 1:00 2:00 3:00 4:00"))

  (add-hook 'org-mode-hook
            '(lambda ()
               (setq line-spacing 0.2))) ;; Add more line padding for readability

  (add-hook 'org-mode-hook
            '(lambda ()
               "Beautify Org Checkbox Symbol"
               (push '("[ ]" . "☐") prettify-symbols-alist)
               (push '("[x]" . "☒") prettify-symbols-alist)
               (prettify-symbols-mode)
               ))
  )

(use-package org-bullets
  :init
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(require 'org-install)
(setq org-modules '(org-habit org-info org-tempo))
(org-load-modules-maybe t)

(setq org-habit-graph-column 105)

(defun org-make-habit ()
  (interactive)
  (org-set-property "STYLE" "habit"))

(use-package org-cliplink
  :bind ("C-x p i" . org-cliplink))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (R . t)
   (python . t)
   (shell . t)
   (latex . t)
   (julia . t)
   (dot . t)
   (sql . t)))

(use-package toc-org
    :init
    (add-hook 'org-mode-hook 'toc-org-mode))

(when (is-mac-p)
  (setq keybase-ref "/Volumes/Keybase\ (yizhx)/private/yizhe/references/"))

(when (is-linux-p)
  (setq keybase-ref "/keybase/private/yizhe/references/"))

(defun my/org-ref-open-pdf-at-point ()
  "Open the pdf for bibtex key under point if it exists."
  (interactive)
  (let* ((results (org-ref-get-bibtex-key-and-file))
         (key (car results))
         (pdf-file (funcall org-ref-get-pdf-filename-function key)))
    (if (file-exists-p pdf-file)
    (funcall bibtex-completion-pdf-open-function (car (bibtex-completion-find-pdf key)))
      (message "No PDF found for %s" key))))

(setq org-ref-open-pdf-function 'my/org-ref-open-pdf-at-point)
(use-package org-ref
  :init
  ;; setup org-ref
  (setq org-ref-default-bibliography "~/Documents/megrez/library.bib"
        org-ref-bibliography-notes "~/Documents/megrez/note.org"
        org-ref-pdf-directory keybase-ref
        org-ref-open-pdf-function 'my/org-ref-open-pdf-at-point))

(use-package org-noter
  :after org
  :config
  (setq org-noter-default-notes-file-names '("todo.org")
        org-noter-notes-search-path '("~/Documents/megrez")
        org-noter-separate-notes-from-heading t))

(when (is-mac-p)
  (setq org-noter-set-doc-split-fraction 0.7))

(defun org-ref-noter-at-point ()
      "Open the pdf for bibtex key under point if it exists."
      (interactive)
      (let* ((results (org-ref-get-bibtex-key-and-file))
             (key (car results))
             (pdf-file (funcall org-ref-get-pdf-filename-function key)))
        (if (file-exists-p pdf-file)
            (progn
              (find-file-other-window pdf-file)
              (org-noter))
          (message "no pdf found for %s" key))))

(add-to-list 'org-ref-helm-user-candidates
             '("Org-Noter notes" . org-ref-noter-at-point))

;; appending a new path to existing path
(when (is-mac-p)
  (setenv "PKG_CONFIG_PATH"
          (concat
           "/usr/local/Cellar/zlib/1.2.8/lib/pkgconfig" ":"
           "/usr/local/opt/libffi/lib/pkgconfig" ":"
           "/usr/local/lib/pkgconfig" ":"
           "/opt/X11/lib/pkgconfig" ":"
           (getenv "PKG_CONFIG_PATH")
           )))

(use-package pdf-tools
  :ensure t
  :config
  (custom-set-variables
    '(pdf-tools-handle-upgrades nil)) ; Use brew upgrade pdf-tools instead.
  (setq pdf-info-epdfinfo-program "/usr/local/bin/epdfinfo"))
(pdf-tools-install)

(setq org-agenda-inhibit-startup nil
      org-agenda-show-future-repeats nil
      org-agenda-start-on-weekday nil
      org-agenda-skip-deadline-if-done t
      org-agenda-skip-scheduled-if-done t)

(unbind-key "C-c [")
(unbind-key "C-c ]")

(use-package org-super-agenda
  :init
  (org-super-agenda-mode)
  (defun my-org-super-agenda ()
    (interactive)
    (let ((org-super-agenda-groups
           '((:name "Today"
                    :time-grid t)
             (:name "Follow-Up" ;; monastery work
                    :tag "followup")
             ;; After the last group, the agenda will display items that didn't
             ;;m atch any of these groups, with the default order position of 99
             ;; To prevent this, add this code:
             ;; (:discard (:anything t))
             )))
      (org-agenda nil "a")))

  (defun my-org-super-agenda-today ()
    (interactive)
    (progn
      (my-org-super-agenda)
      (org-agenda-day-view)))

  (bind-keys ("C-c 1" . my-org-super-agenda-today)
             ("C-c 0" . my-org-super-agenda))

  :config

  ;; Enable folding
  (use-package origami
    :bind (:map org-super-agenda-header-map
                ("TAB" . origami-toggle-node))
    :hook ((org-agenda-mode . origami-mode))))

(setq org-agenda-files (list "~/Documents/megrez/todo.org"
                             "~/Documents/megrez/inbox.org"))

(defun open-agenda ()
  "Opens the org-agenda."
  (interactive)
  (let ((agenda "*Org Agenda*"))
    (if (equal (get-buffer agenda) nil)
        (org-agenda-list)
      (unless (equal (buffer-name (current-buffer)) agenda)
        (switch-to-buffer agenda))
      (org-agenda-redo t)
      (beginning-of-buffer))))

(bind-key "<f5>" 'my-org-super-agenda)

(add-hook 'org-agenda-finalize-hook (lambda () (delete-other-windows)))

(defun org-buffer-todo ()
  (interactive)
  "Creates a todo-list for the current buffer. Equivalent to the sequence: org-agenda, < (restrict to current buffer), t (todo-list)."
  (progn
    (org-agenda-set-restriction-lock 'file)
    (org-todo-list)))

(defun org-buffer-agenda ()
  (interactive)
  "Creates an agenda for the current buffer. Equivalent to the sequence: org-agenda, < (restrict to current buffer), a (agenda-list)."
  (progn
    (org-agenda-set-restriction-lock 'file)
    (org-agenda-list)))

(defun org-buffer-day-agenda ()
  (interactive)
  "Creates an agenda for the current buffer. Equivalent to the sequence: org-agenda, < (restrict to current buffer), a (agenda-list), d (org-agenda-day-view)."
  (progn
    (org-agenda-set-restriction-lock 'file)
    (org-agenda-list)
    (org-agenda-day-view))) ;; Maybe I should try writing a Emacs Lisp macro for this kind of thing!

(bind-key "y" 'org-agenda-todo-yesterday org-agenda-mode-map)

(add-to-list 'org-agenda-custom-commands
             '("L" "Timeline"
               ((agenda
                 ""
                 ((org-agenda-span 7)
                  (org-agenda-prefix-format '((agenda . " %1c %?-12t% s"))))))))

(add-to-list 'org-agenda-custom-commands
             '("u" "Unscheduled TODOs"
               ((todo ""
                      ((org-agenda-overriding-header "\nUnscheduled TODO")
                       (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp 'todo '("DONE" "CANCELLED" "MAYBE" "WAITING" "SOMEDAY"))))))) t)

(setq org-capture-templates
      '(("t" "Task" entry (file "~/Documents/megrez/inbox.org")
         "* TODO %?\n")))

(setq org-refile-targets '((("~/Documents/megrez/todo.org" "~/Documents/megrez/archive.org") :maxlevel . 3))
      ;; org-refile-use-cache t
      org-refile-use-outline-path t)

(defun bh/verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'bh/verify-refile-target)

(setq org-log-done 'time
      org-clock-idle-time nil
      org-clock-continuously nil
      org-clock-persist t
      org-clock-in-switch-to-state "STARTED"
      org-clock-in-resume nil
      org-clock-report-include-clocking-task t
      org-clock-out-remove-zero-time-clocks t
      ;; Too many clock entries clutter up a heading
      org-log-into-drawer t
      org-clock-into-drawer 1)

(defun bh/remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line 0)
    (org-remove-empty-drawer-at (point))))

(add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)

(defhydra hydra-org-clock (:color blue :hint nil)
  "
Clock   In/out^     ^Edit^   ^Summary     (_?_)
-----------------------------------------
        _i_n         _e_dit   _g_oto entry
        _c_ontinue   _q_uit   _d_isplay
        _o_ut        ^ ^      _r_eport
      "
  ("i" org-clock-in)
  ("o" org-clock-out)
  ("c" org-clock-in-last)
  ("e" org-clock-modify-effort-estimate)
  ("q" org-clock-cancel)
  ("g" org-clock-goto)
  ("d" org-clock-display)
  ("r" org-clock-report)
  ("?" (org-info "Clocking commands")))

(defhydra hydra-org-agenda-clock (:color blue :hint nil)
  "
Clock   In/out^
-----------------------------------------
        _i_n
        _g_oto entry
        _o_ut
        _q_uit
      "
  ("i" org-agenda-clock-in)
  ("o" org-agenda-clock-out)
  ("q" org-agenda-clock-cancel)
  ("g" org-agenda-clock-goto))

(bind-keys ("C-c w" . hydra-org-clock/body)
           :map org-agenda-mode-map
           ("C-c w" . hydra-org-agenda-clock/body))

(defun open-todo-file ()
  (interactive)
  (find-file "~/Documents/megrez/todo.org"))

(bind-key "C-c t" 'open-todo-file)

(add-hook 'org-mode-hook
              (lambda ()
                (push '("TODO"  . ?▲) prettify-symbols-alist)
                (push '("DONE"  . ?✓) prettify-symbols-alist)
                (push '("CANCELLED"  . ?✘) prettify-symbols-alist)
                (push '("QUESTION"  . ??) prettify-symbols-alist)))

(setq browse-url-browser-function
      (cond ((is-mac-p) 'browse-url-default-macosx-browser)
            ((is-linux-p) 'browse-url-default-browser)))

(bind-key "C-c B" 'browse-url-at-point)

(setq browse-url-browser-function 'eww-browse-url)

(use-package expand-region
  :bind (("C-@" . er/expand-region)
         ("C-=" . er/expand-region)
         ("M-3" . er/expand-region)))

(pending-delete-mode t)

(use-package selected
  :commands selected-minor-mode
  :init
  (setq selected-org-mode-map (make-sparse-keymap))
  (selected-global-mode 1)
  :bind (:map selected-keymap
              ("e" . er/expand-region)
              ("i" . indent-region)
              ("l" . downcase-region)
              ("m" . apply-macro-to-region-lines)
              ("q" . selected-off)
              ("r" . reverse-region)
              ("s" . sort-lines)
              ("u" . upcase-region)
              ("w" . count-words-region)
              ("y" . yank)
              :map selected-org-mode-map
              ("t" . org-table-convert-region)))

(use-package goto-addr
  :hook ((compilation-mode . goto-address-mode)
         (prog-mode . goto-address-prog-mode)
         (eshell-mode . goto-address-mode)
         (shell-mode . goto-address-mode))
  :bind (:map goto-address-highlight-keymap
              ("C-c C-o" . goto-address-at-point))
  :commands (goto-address-prog-mode
             goto-address-mode))

(use-package emojify
  :init (global-emojify-mode))

(use-package linum-relative
  :init
  (setq linum-format 'linum-relative)
  :config
  (setq linum-relative-current-symbol ""))

(use-package aggressive-indent
  :init
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'scala-mode)
  (unbind-key "C-c C-q" aggressive-indent-mode-map))

(defun find-config-file ()
  "Edit my emacs config file"
  (interactive)
  (let ((config-file "~/.emacs.d/config.org"))
    (find-file config-file)))

(bind-key "C-c e" 'find-config-file)

(defun find-init-file ()
  "Edit my init file in another window."
  (interactive)
  (let ((mwf-init-file "~/.emacs.d/init.el"))
    (find-file mwf-init-file)))

(defun reload-init-file ()
  "Reload my init file."
  (interactive)
  (load-file user-init-file))

(bind-key "C-c M-i" 'reload-init-file)

;; activate debugging
(setq debug-on-error nil
      debug-on-signal nil
      debug-on-quit nil)
